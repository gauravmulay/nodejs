var fs = require('fs');

// sync
var readMe = fs.readFileSync('readMe.txt', 'utf8');
console.log(readMe);
fs.writeFileSync('writeMe.txt', readMe);

//async
fs.readFile('readme.txt', 'utf8', function(err, data) {
  console.log(data);
});

console.log('test');

//fs.unlink('writeMe.txt');

fs.mkdirSync('stuff1');
fs.rmdirSync('stuff1');