const fs = require('fs');
const validator = require('validator');
const chalk = require('chalk');
const consoleMessage = require('./notes.js');

// Append the file
fs.appendFileSync('notes.txt', 'This is a advanced app');

// Call the basic function to print
console.log(consoleMessage());

// Use validator library
console.log(validator.isURL('http://google.com'));


// Ussage of chalk lib
console.log(chalk.blue('Succes'));

// Getting inputs from user
const command = process.argv[2];

if (command == 'add') {
    console.log('Adding note');
}