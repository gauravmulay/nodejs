import React from "react";

// function Greet() {
//     return <h1>Hello Gaurav</h1>
// }

const Greet = ({ name, test }) => {
  return (
    <div>
      <h1>
        Hello {name} aka {test}
      </h1>
    </div>
  );
};
export default Greet;
