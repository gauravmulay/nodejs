import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Greet from "./components/Greet";
import Welcome from "./components/Welcome";
import Hello from "./components/Hello";
import Message from "./components/Message";
import Counter from "./components/Counter";

function App() {
  return (
    <div className="App">
      {/* <Greet name = "Papya" test = "Mindstix">
        <p>This is a children props</p>
      </Greet>
      <Greet name = "Gaurav" test = "Mulay">
        <button>Submit</button>
      </Greet>
      <Welcome name = "Papya" test = "Mindstix"></Welcome>
      <Welcome name = "Gaurav" test = "Mulay"></Welcome>
      <Welcome name = "Sandip" test = "Karekar"></Welcome> */}
      {/* <Message/> */}
      <Counter />
      <Greet name="Gaurav" test="Mulay" />{" "}
    </div>
  );
}

export default App;
